# Heart Disease Prediction C++ (K-Nearest Neighbor)

Dataset consists of information regarding patients with the following attributes :
1. **Age** (in years)
2. **Sex** (1 = male, 0 = female)
3. **Chest pain type** (category 1, 2, 3, 4)
4. **Blood pressure** (in mmHg)
5. **Cholesterol level** (in mg/dL)
6. **Blood sugar > 120 mg/dL** (1 = yes, 0 = no)
7. **Electrocardiographic (ECG) results** (0 = normal, 1 = slight abnormality, 2 = definite abnormality)
8. **Maximum heart rate** (beats per minute)
9. **Exercise-induced angina** (1 = yes, 0 = no)
10. **Exercise-induced ST Depression** (real-valued number)
11. **ST Segment peak slope** (1 = upslope, 2 = flat, 3 = downslope)
12. **Fluoroscopy result** (number of major vessels (0-3) colored by fluoroscopy)
13. **Thalassemia** (3 = normal; 6 = fixed defect; 7 = reversible defect)
14. _**Heart disease**_ (0 = no heart disease, 1 = heart disease)

The focus of the model will be to predict whether a patient has a heart disease or not based on the patient's first 13 attributes.

The heartdisease-train.csv dataset is used to train the prediction model, which consists of :
1. 270 rows of data, with each row representing the attributes of a unique patient.
2. 1st to 13th column represents the patient's attributes, in the respective order as mentioned above.
3. 14th column represents the target attribute, whether the patient has a heart disease or not.
4. Each column is separated by a comma (',').

The heartdisease-test.csv dataset is used to test the accuracy of the prediction models, which consists of :
1. 27 rows of data, with each row representing the attributes of a unique patient.
2. 14 columns with specifications following heartdisease-train.csv.

Note :
1. Project is built in Visual Studio 2010.
2. For academic purposes, most of the algorithms are self-coded albeit not optimized.

# Method : K-Nearest Neighbor

**1. Formulating prediction algorithm**

When predicting a target patient's target attribute (_heart disease_), the euclidean distance between that target patient's attributes and other known patients' attributes will be calculated.

![](Screenshots/euclidean-n-axis.png)

Then, k number of patients with the smallest ecludiean distance with the target patient will be taken and a majority vote will be cast among them.

The result of the vote determines the target attribute (_heart disease_) of the target patient.

For example, with k = 3, patient D is the closest with the following patients :
- Patient A ("yes")
- Patient B ("no")
- Patient C ("yes")

According to the pool above, patient D will receive a majority verdict "yes", indicating that he has a heart disease.

**2. Formulating accuracy metrics**

To measure the accuracy of the predictions, 4 metrics will be used :
- Accuracy, which is simply = (number_of_correct_predictions) / (total_number_of_predictions)
- F1-Score
- Precision
- Recall

![](Screenshots/f1-precision-recall.png)

**3. Finding the best "k"**

The value for each accuracy metrics will be calculated for each value of k, where 1 <= k <= column_size (13).

K-value is capped at column_size (13) as setting the value any higher will produce way too generalized results.

The k-value with the best overall results will be used to predict the test-dataset.

The training is done by using 90% of the train-dataset for the model, and the remaining 10% for the measurement.

![](Screenshots/k-comparison.png)

From the results above, k = 3 is chosen as it delivers the best overall results.

**4. Calculate prediction accuracy using test dataset**

![](Screenshots/test-result.png)